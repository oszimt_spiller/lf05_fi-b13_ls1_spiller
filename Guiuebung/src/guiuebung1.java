import java.awt.EventQueue;
import java.awt.Font; // Um die Schrift im Label zu modifizieren

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JColorChooser; //Fuer Farbe waehlen Fenster
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color; // Farbe waehlen

public class guiuebung1 {

	private JFrame frmForm;
	private JTextField txtEingabe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					guiuebung1 window = new guiuebung1();
					window.frmForm.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public guiuebung1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmForm = new JFrame();
		frmForm.setTitle("Form_aendern");
		frmForm.setBounds(100, 100, 325, 550);
		frmForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmForm.getContentPane().setLayout(null);

		JLabel lblOberesTextfeld = new JLabel("Dieser Text soll ge\u00E4ndert werden");
		lblOberesTextfeld.setHorizontalAlignment(SwingConstants.CENTER);
		lblOberesTextfeld.setBounds(10, 0, 286, 55);
		frmForm.getContentPane().add(lblOberesTextfeld);

		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00C4ndern");
		lblAufgabe1.setHorizontalAlignment(SwingConstants.LEFT);
		lblAufgabe1.setBounds(10, 56, 414, 14);
		frmForm.getContentPane().add(lblAufgabe1);

		JButton btngBackRot = new JButton("Rot");
		btngBackRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmForm.getContentPane().setBackground(Color.RED);

			}
		});
		btngBackRot.setBounds(10, 73, 89, 23);
		frmForm.getContentPane().add(btngBackRot);

		JButton btnBackGruen = new JButton("Gr\u00FCn");
		btnBackGruen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmForm.getContentPane().setBackground(Color.GREEN);
			}
		});
		btnBackGruen.setBounds(109, 73, 89, 23);
		frmForm.getContentPane().add(btnBackGruen);

		JButton btnBackBlau = new JButton("Blau");
		btnBackBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmForm.getContentPane().setBackground(Color.BLUE);
			}
		});
		btnBackBlau.setBounds(208, 73, 89, 23);
		frmForm.getContentPane().add(btnBackBlau);

		JButton btnBackGelb = new JButton("Gelb");
		btnBackGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmForm.getContentPane().setBackground(Color.YELLOW);
			}
		});
		btnBackGelb.setBounds(10, 100, 89, 23);
		frmForm.getContentPane().add(btnBackGelb);

		JButton btnBackStandardfarbe = new JButton("Standard");
		btnBackStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmForm.getContentPane().setBackground(new Color(0xEEEEEE));
			}
		});
		btnBackStandardfarbe.setBounds(109, 100, 89, 23);
		frmForm.getContentPane().add(btnBackStandardfarbe);

		JButton btnBackfarbeWaehlen = new JButton("W\u00E4hlen");
		btnBackfarbeWaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Import JColorChooser um ein Abfragefenster zu �ffnen, welche die gewaehlte
				// Farbe als newColor speicher Quelle:
				// https://stackoverflow.com/questions/26565166/how-to-display-a-color-selector-when-clicking-a-button
				// 20.06.2022
				Color newColor = JColorChooser.showDialog(null, "Choose a color", Color.RED);
				frmForm.getContentPane().setBackground(newColor);
			}

		});
		btnBackfarbeWaehlen.setBounds(208, 100, 89, 23);
		frmForm.getContentPane().add(btnBackfarbeWaehlen);

		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(10, 134, 286, 14);
		frmForm.getContentPane().add(lblAufgabe2);

		JButton btnTextArial = new JButton("Arial");
		btnTextArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setFont(new Font("Arial", Font.PLAIN, lblOberesTextfeld.getFont().getSize()));
//get die momentane Fontsize, damit das Ganze mit der Folgeaufgabe kompatibel ist
			}
		});
		btnTextArial.setBounds(10, 151, 89, 23);
		frmForm.getContentPane().add(btnTextArial);

		JButton btnTextComic = new JButton("Comic Sans ");
		btnTextComic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setFont(new Font("Comic Sans MS", Font.PLAIN, lblOberesTextfeld.getFont().getSize()));
				// get die momentane Fontsize, damit das Ganze mit der Folgeaufgabe kompatibel
				// ist
			}
		});
		btnTextComic.setBounds(109, 151, 89, 23);
		frmForm.getContentPane().add(btnTextComic);

		JButton btnTextCourier = new JButton("Courier ");
		btnTextCourier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setFont(new Font("Courier New", Font.PLAIN, lblOberesTextfeld.getFont().getSize()));
				// get die momentane Fontsize, damit das Ganze mit der Folgeaufgabe kompatibel
				// ist
			}
		});
		btnTextCourier.setToolTipText("");
		btnTextCourier.setBounds(208, 151, 89, 23);
		frmForm.getContentPane().add(btnTextCourier);

		txtEingabe = new JTextField();
		txtEingabe.setText("Hier bitte Text eingeben");
		txtEingabe.setBounds(10, 180, 286, 20);
		frmForm.getContentPane().add(txtEingabe);
		txtEingabe.setColumns(10);

		JButton btnLabelSchreiben = new JButton("Ins Label schreiben");
		btnLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setText(txtEingabe.getText());
			}
		});
		btnLabelSchreiben.setBounds(10, 205, 140, 23);
		frmForm.getContentPane().add(btnLabelSchreiben);

		JButton btnLabelDelete = new JButton("Text im Label l\u00F6schen");
		btnLabelDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setText("");
				// L�schung realisiert durch �bergabe eines leeren Strings
			}
		});
		btnLabelDelete.setBounds(156, 205, 140, 23);
		frmForm.getContentPane().add(btnLabelDelete);

		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setBounds(10, 239, 286, 14);
		frmForm.getContentPane().add(lblAufgabe3);

		JButton btnTextRot = new JButton("Rot");
		btnTextRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setForeground(Color.RED);
			}
		});
		btnTextRot.setBounds(10, 257, 89, 23);
		frmForm.getContentPane().add(btnTextRot);

		JButton btnTextBlau = new JButton("Blau");
		btnTextBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setForeground(Color.BLUE);
			}
		});
		btnTextBlau.setBounds(109, 257, 89, 23);
		frmForm.getContentPane().add(btnTextBlau);

		JButton btnTextSchwarz = new JButton("Schwarz");
		btnTextSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setForeground(Color.BLACK);
			}
		});
		btnTextSchwarz.setBounds(207, 257, 89, 23);
		frmForm.getContentPane().add(btnTextSchwarz);

		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblAufgabe4.setBounds(10, 291, 286, 14);
		frmForm.getContentPane().add(lblAufgabe4);

		JButton btnTextGroesser = new JButton("+");
		btnTextGroesser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Keine Ahnung ob das sauber ist, aber es funktioniert. Ich hole mir einfach
				// die momentanen Werte der Font KLasse und inkrementiere FontSize um eins
				lblOberesTextfeld.setFont(new Font(lblOberesTextfeld.getFont().getFontName(),
						lblOberesTextfeld.getFont().getStyle(), lblOberesTextfeld.getFont().getSize() + 1));
			}
		});
		btnTextGroesser.setBounds(10, 310, 140, 23);
		frmForm.getContentPane().add(btnTextGroesser);

		JButton btnTextKleiner = new JButton("-");
		btnTextKleiner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Keine Ahnung ob das sauber ist, aber es funktioniert. Ich hole mir einfach
				// die momentanen Werte der Font KLasse und dekrementiere FontSize um eins
				lblOberesTextfeld.setFont(new Font(lblOberesTextfeld.getFont().getFontName(),
						lblOberesTextfeld.getFont().getStyle(), lblOberesTextfeld.getFont().getSize() - 1));
			}
		});
		btnTextKleiner.setBounds(156, 310, 140, 23);
		frmForm.getContentPane().add(btnTextKleiner);

		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setBounds(10, 344, 286, 14);
		frmForm.getContentPane().add(lblAufgabe5);

		JButton btnTextLinks = new JButton("links");
		btnTextLinks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnTextLinks.setBounds(10, 364, 89, 23);
		frmForm.getContentPane().add(btnTextLinks);

		JButton btnTextMitte = new JButton("zentriert");
		btnTextMitte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnTextMitte.setBounds(109, 364, 89, 23);
		frmForm.getContentPane().add(btnTextMitte);

		JButton btnTextRechts = new JButton("rechts");
		btnTextRechts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOberesTextfeld.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnTextRechts.setBounds(207, 364, 89, 23);
		frmForm.getContentPane().add(btnTextRechts);

		JLabel lblAufgabe6 = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe6.setBounds(10, 398, 286, 14);
		frmForm.getContentPane().add(lblAufgabe6);

		JButton btnProgrammBeenden = new JButton("EXIT");
		btnProgrammBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnProgrammBeenden.setBounds(10, 417, 286, 55);
		frmForm.getContentPane().add(btnProgrammBeenden);
	}
}
