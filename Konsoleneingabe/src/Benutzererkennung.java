import java.util.Scanner; // Import der Klasse Scanner 


public class Benutzererkennung {

	public static void main(String[] args) // Hier startet das Programm 
	  { 
	     
	    // Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	     
	    System.out.print("Herzlich willkommen. Bitte geben Sie Ihren Vornamen ein: ");    
	     
	    // Die Variable vorname speichert den Vornamen 
	    String vorname = myScanner.next();  
	     
	    System.out.print("Bitte geben Sie jetzt Ihren Nachnamen ein: "); 
	     
	    // Die Variable vnachname speichert den Nachnamen 
	    String nachname = myScanner.next();  
	    
	    System.out.print("Als letztes ben�tigen wir noch Ihr Alter: "); 
	    
	    // Die Variable alter speichert das Alter
	    byte alter = myScanner.nextByte(); 

	    
	    //Ausgabe der Eingaben
	    System.out.print("Sie hei�en " + vorname + " " + nachname + " und sind " + alter + " Jahre alt.");
	    
	
	    myScanner.close(); 
	     
	  }    
}
