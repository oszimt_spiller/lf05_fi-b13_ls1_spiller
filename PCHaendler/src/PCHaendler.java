import java.util.Scanner;

public class PCHaendler {

	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		return anzahl * preis;
	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}

	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

	public static String liesString() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("was m�chten Sie bestellen?");
		String artikel = myScanner.next();
		return artikel;
	}

	public static int liesInt() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();
		return anzahl;
	}

	public static double liesPreis() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();
		return preis;
	}

	public static double liesMwst() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();
		return mwst;
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString();
		int anzahl = liesInt();
		double mwst = liesMwst();
		double preis = liesPreis();

		/*
		 * System.out.println("was m�chten Sie bestellen?"); String artikel =
		 * myScanner.next();
		 * 
		 * System.out.println("Geben Sie die Anzahl ein:"); int anzahl =
		 * myScanner.nextInt();
		 * 
		 * System.out.println("Geben Sie den Nettopreis ein:"); double preis =
		 * myScanner.nextDouble();
		 * 
		 * System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		 * double mwst = myScanner.nextDouble();
		 */
		// Verarbeiten
		// double nettogesamtpreis = anzahl * preis;
		// double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		// System.out.println("\tRechnung");
		// System.out.printf("\t\t Netto: %-20s %6d %10.2f %n", artikel, anzahl,
		// nettogesamtpreis);
		// System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel,
		// anzahl, bruttogesamtpreis, mwst, "%");

		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}

}
