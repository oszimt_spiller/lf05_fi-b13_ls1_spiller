import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		/*
		 * Schreiben Sie ein Programm, das zu einer Zahl n <= 20 die Fakult�t n!
		 * ermittelt.
		 * 
		 * Es gilt: n! = 1 * 2 * ... * (n-1) * n sowie 0! = 1
		 * 
		 * z.B. ist 3! = 1 * 2 * 3 = 6
		 */
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben sie eine Zahl von 0 bis 20 ein, " + "von der die Fakult�t berechnet werden soll");
		int fak = tastatur.nextInt();
		if (fak == 0) {
			System.out.print("1");
			// falls die eingabe 0 ist
		} else { // falls die eingabe >0 ist
			int ergebnis = 1;
			while (fak > 0) {
				ergebnis = ergebnis * fak;
				fak--;
			}
			System.out.print(ergebnis);
		}
	}

}
