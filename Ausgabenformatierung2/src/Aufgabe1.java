
public class Aufgabe1 {

	public static void main(String[] args) {
		String s = "*";
		//mein Versuch an der Zusatzaufgabe ohne Leerzeichen
		System.out.printf("\t%s%n",s + s);
		System.out.printf("%s\t",s);
		System.out.printf("\t%s\n",s);
		System.out.printf("%s\t",s);
		System.out.printf("\t%s\n",s);
		System.out.printf("\t%s%n",s + s);
		
		//mein Versuch mit Leerzeichen
		System.out.printf("%s%n","   "+s + s);
		System.out.printf("%s%n",s + "      " + s);
		System.out.printf("%s%n",s + "      " + s);
		System.out.printf("%s%n","   " +s + s);
	}

}
