
public class Temperaturtabelle {

	public static void main(String[] args) {
		// die ungerundeten Daten
		double f1 = -28.8889;
		double f2 = -23.3333;
		double f3 = -17.7778;
		double f4 = -6.6667;
		double f5 = -1.1111;
		
		int d1 = 20; // eigentlicher Wert -20
		int d2 = 10; // eigentlicher Wert -10
		int d3 = 0;
		int d4 = 20;
		int d5 = 30;
		//Kopfzeile
		System.out.printf("%-12s","Fahrenheit" );
		System.out.print("|");
		System.out.printf("%10s%n","Celsius" );
		System.out.printf("%.24s%n","----------------------------------------------------" );
		/* die Temperatureintr�ge
		 %10.2 10 steht f�r die Breite und in dem Fall rechtsb�ndig, -10 w�re linksb�ndig
		 %10.2 2 steht f�r Anzahl der Nachkommastellen (bei double) und f�r maximale L�nge des 
		 Strings bei Strings 
		*/
		System.out.printf("%-12s",-d1 );
		System.out.print("|");
		System.out.printf("%10.2f%n",f1 );
		
		System.out.printf("%-12s",-d2 );
		System.out.print("|");
		System.out.printf("%10.2f%n",f2 );
		
		System.out.printf("%-12s","+"+d3 ); 
		System.out.print("|");
		System.out.printf("%10.2f%n",f3 );
		
		System.out.printf("%-12s","+"+d4 );
		System.out.print("|");
		System.out.printf("%10.2f%n",f4 );
		
		System.out.printf("%-12s","+"+d5 );
		System.out.print("|");
		System.out.printf("%10.2f%n",f5 );

	}

}
