
public class Konto {
	private String iban;
	private int kontonummer;
	private double balance;

	public Konto() {
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getIban() {
		return this.iban;
	}

	public void setKontonummer(int kontonummer) {
		this.kontonummer = kontonummer;
	}

	public int getKontonummer() {
		return this.kontonummer;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getBalance() {
		return this.balance;
	}

	public void ueberweisen(Konto kx, double amount) {
		this.balance -= amount;
		kx.balance += amount;
	}
}
