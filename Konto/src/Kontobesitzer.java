
public class Kontobesitzer {
	private String vorname;
	private String nachname;
	private Konto[] konten = new Konto[2];

//Ein Kontobesitzer hat immer 2 Konten, wird durch den Konstruktor realisiert
	public Kontobesitzer(Konto ka, Konto kb) {
		konten[0] = ka;
		konten[1] = kb;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getVorname() {
		return this.vorname;
	}

	public String getNachname() {
		return this.nachname;
	}

//Erzeuge eine Uebersicht der Kontoinformationen
	public void gesamtUebersicht() {
		System.out.println("Sie sind Inhaber folgender Konten: \n \nErstes Konto \n" + konten[0].getKontonummer()
				+ " \n" + konten[0].getIban() + "\n" + konten[0].getBalance() + "�\n");
		System.out.println("Zweites Konto \n" + konten[1].getKontonummer() + "\n" + konten[1].getIban() + "\n"
				+ konten[1].getBalance() + "�\n");
	}

	// Addiert den Kontostand beider Konten und gibt den Wert aus
	public void gesamtGeld(Konto ka, Konto kb) {
		double gesamt = ka.getBalance() + kb.getBalance();
		System.out.print("Ihr Gesamtguthaben �ber alle Konten verteilt ist: " + gesamt);
	}

}
