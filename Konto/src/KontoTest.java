
public class KontoTest {

	public static void main(String[] args) {
		Konto k1 = new Konto();
		Konto k2 = new Konto();
		Kontobesitzer rami = new Kontobesitzer(k1, k2);
		// set-Methoden testen
		k1.setBalance(1000);
		k2.setBalance(2000);

		k1.setIban("11111111");
		k2.setIban("222222");

		k1.setKontonummer(3333333);
		k2.setKontonummer(444444);

		// Bildschirmausgabe und dadurch Test der get-Methoden
		System.out.println("Guthaben: " + k1.getBalance() + " " + k2.getBalance());
		System.out.println("Kontonummer: " + k1.getKontonummer() + " " + k2.getKontonummer());
		System.out.println("Iban: " + k1.getIban() + " " + k2.getIban());

		// Test der normalen Methoden
		System.out.println("Guthaben k1 und k2 vorher: " + k1.getBalance() + " " + k2.getBalance());
		k1.ueberweisen(k2, 500);
		System.out.println("Guthaben k1 und k2 nach Ueberweisung von 500�: " + k1.getBalance() + " " + k2.getBalance());

		// Kontobesitzer testen
		rami.setVorname("Rami");
		rami.setNachname("Spiller");
		System.out.println("Kontoinhaber: " + rami.getVorname() + " " + rami.getNachname());

		rami.gesamtUebersicht();
		rami.gesamtGeld(k1, k2);
	}

}
