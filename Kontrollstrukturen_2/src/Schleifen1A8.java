import java.util.Scanner;

public class Schleifen1A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben sie die L�nge der Seiten des Quadrats in Einheiten an.");
		//seitenl�nge
		int lang = tastatur.nextInt();
		//der vertikale zwischenraum zwischen der oberen und unteren Seite
		int zwischen = lang - 2;
		//Erste Zeile
		for (int k = 0; k < lang; k++) {
			System.out.printf("*  ");
			
		}
		System.out.println();
		//Zwischenzeilen mit Hohlraum wenn seitenl�nge gr��er als 2
		if(lang > 2) {
		for (int i = 2; i < lang; i++) {
			//Der linke Punkt

			System.out.printf("*  ");
			for (int j = 2; j < lang; j++) {
				// Die leeren Punkte dazwischen
				System.out.print("   ");
			}
			//der rechte Punkt und ein Zeilembruch f�r die n�chste Reihe
			System.out.print("*  ");
			System.out.println();

		}
		}
		//Letzte Zeile wenn seitenl�nge gr��er als 1
		if(lang > 1) {
		for (int l = 0; l < lang; l++) {
			System.out.printf("*  ");
		}
		}
	}
}
