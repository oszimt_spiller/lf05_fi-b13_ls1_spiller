import java.util.Scanner;

public class FallunterscheidungenA5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Welche Gr��e wollen sie berechnen? \n\t R (1) \n\t U (2) \n\t I (3)");
		int erstewahl = tastatur.nextInt();
		//While-Schleife l�uft so lange, bis die Eingabe richtig ist
		int failcheck = 0;
		while (failcheck == 0) {
			//Je nach dem was berechnet werden soll gibt es cases
			switch (erstewahl) {
			case 1:

				System.out.println("Geben sie nun den Wert f�r die Spannung in Volt an");
				double spannung = tastatur.nextDouble();
				System.out.println("Geben sie nun den Wert f�r die Stromst�rke in Ampere an");
				double strom = tastatur.nextDouble();
				double widerstand = spannung / strom;
				System.out.println("Der Widerstand betr�gt: " + widerstand + " Ohm");
				failcheck = 1;
				break;
			case 2:

				System.out.println("Geben sie nun den Wert f�r die Stromst�rke in Ampere an");
				strom = tastatur.nextDouble();
				System.out.println("Geben sie nun den Wert f�r den Widerstand in Ohm an");
				widerstand = tastatur.nextDouble();
				spannung = widerstand * strom;
				System.out.println("Die Spannung betr�gt: " + spannung + " V");
				failcheck = 1;
				break;
			case 3:

				System.out.println("Geben sie nun den Wert f�r die Spannung in Volt an");
				spannung = tastatur.nextDouble();
				System.out.println("Geben sie nun den Wert f�r den Widerstand in Ohm an");
				widerstand = tastatur.nextDouble();
				strom = spannung / widerstand;
				System.out.println("Die Stromst�rke betr�gt: " + strom + " A");
				failcheck = 1;
				break;
			default:
				System.out.println("Falsche Eingabe!");
			}
		}

	}

}
