import java.util.Scanner;

public class Schleifen2A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben sie eine ganze Zahl zwischen 2 und 9 ein.");
		int wahl = tastatur.nextInt();
		int eintrag = 0;
		String wahlZeichen = "" + wahl;
		for (int i = 1; i < 11; i++) {
			for (int j = 1; j < 11; j++) {
				// falls durch Zahl teilbar
				if (eintrag % wahl == 0) {

					System.out.printf("%-4s", "*");
					eintrag += 1;
				} else if (eintrag % 10 + ((eintrag - eintrag % 10) / 10) == wahl) {
					// falls die Quersumme = 4 ist
					// erste summand ist die zweite Stelle der Quersumme und der Rest ist die Erste
					// Stelle
					System.out.printf("%-4s", "*");
					eintrag += 1;
				} else if ((eintrag - eintrag % 10) / 10 == wahl || (eintrag % 10 == wahl)) {
					// wenn Ziffer enthalten ist, der erste Teil f�r die Erste Ziffer und der zweite f�r die zweite Ziffer
					System.out.printf("%-4s", "*");
					eintrag += 1;
				} else {
					// Wenn keine Bedingung zutrifft soll die Zahl ebgebildet werden
					System.out.printf("%-4d", eintrag);
					eintrag += 1;
				}
			}
			System.out.println();

		}
	}

}
