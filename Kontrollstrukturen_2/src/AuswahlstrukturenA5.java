import java.util.Scanner;

public class AuswahlstrukturenA5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Damit wir mit der Berechnung ihres BMIs anfangen k�nnen, brauchen wir ein paar Daten.");
		//Geschlechtabfrage per int 1 oder 2
		System.out.println("Bitte geben sie ihr Geschlecht an \n\t M�nnlich (1)\n\t Weiblich (2)");
		int geschlecht = tastatur.nextInt();

		System.out.println("Bitte geben sie nun ihr Gewicht in Kg an. Runden Sie bitte auf eine ganze Zahl.");
		double gewicht = tastatur.nextDouble();
		System.out.println("Als letztes ben�tigen wir noch ihre K�rpergr��e in m.");
		double groe�e = tastatur.nextDouble();
		//BMI per Formel ausrechnen
		double bmi = gewicht / (groe�e * groe�e);
		//if-Abfrage welche Geschlecht+BMI Kombo zutrifft mit Ausgabe des Ergebnisses
		if (geschlecht == 1 && bmi < 20) {
			System.out.printf("Mit einem BMI von %.2f sind sie untergewichtig", bmi);
		} else if (geschlecht == 1 && bmi >= 20 && bmi <= 25) {
			System.out.printf("Mit einem BMI von %.2f sind sie normalgewichtig", bmi);
		} else if (geschlecht == 1 && bmi > 25) {
			System.out.printf("Mit einem BMI von %.2f sind sie �bergewichtig", bmi);
		}
		if (geschlecht == 2 && bmi < 19) {
			System.out.printf("Mit einem BMI von %.2f sind sie untergewichtig", bmi);
		} else if (geschlecht == 2 && bmi >= 19 && bmi <= 24) {
			System.out.printf("Mit einem BMI von %.2f sind sie normalgewichtig", bmi);
		} else if (geschlecht == 2 && bmi > 24) {
			System.out.printf("Mit einem BMI von %.2f sind sie �bergewichtig", bmi);
		}

	}

}
