package eingabeAusgabe;

/**
 * Variablen.java Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
 * 
 * @author
 * @version
 */
public class Variablen {
	public static void main(String[] args) {
		/*
		 * 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen. Vereinbaren Sie eine
		 * geeignete Variable
		 */
		short zaehler;
		/*
		 * 2. Weisen Sie dem Zaehler den Wert 25 zu und geben Sie ihn auf dem Bildschirm
		 * aus.
		 */
		zaehler = 25;
		System.out.println(zaehler);
		/*
		 * 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt eines Programms
		 * ausgewaehlt werden. Vereinbaren Sie eine geeignete Variable
		 */
		char benutzerEingabe;

		/*
		 * 4. Weisen Sie dem Buchstaben den Wert 'C' zu und geben Sie ihn auf dem
		 * Bildschirm aus.
		 */
		benutzerEingabe = 'C';
		System.out.println(benutzerEingabe);
		/*
		 * 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
		 * notwendig. Vereinbaren Sie eine geeignete Variable
		 */
		long astronomischeVariable;
		/*
		 * 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu und geben Sie sie
		 * auf dem Bildschirm aus.
		 */
		astronomischeVariable = 300000l;
		System.out.println(astronomischeVariable);
		/*
		 * 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung soll
		 * die Anzahl der Mitglieder erfasst werden. Vereinbaren Sie eine geeignete
		 * Variable und initialisieren sie diese sinnvoll.
		 */
		byte anzahlMitglieder = 7; // Ich denke byte also 128 maximale Mitglieder sollte reichen
		/* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus. */
		System.out.println(anzahlMitglieder);
		/*
		 * 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
		 * Vereinbaren Sie eine geeignete Variable und geben Sie sie auf dem Bildschirm
		 * aus.
		 */
		float elekEleLadung = 0.0000000000000000001602F;
		System.out.println(elekEleLadung);
		/*
		 * 10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
		 * Vereinbaren Sie eine geeignete Variable.
		 */
		boolean erfolgreicheZahlung;
		/*
		 * 11. Die Zahlung ist erfolgt. Weisen Sie der Variable den entsprechenden Wert
		 * zu und geben Sie die Variable auf dem Bildschirm aus.
		 */
		erfolgreicheZahlung = true;
		System.out.println(erfolgreicheZahlung);
	}// main
}// Variablen