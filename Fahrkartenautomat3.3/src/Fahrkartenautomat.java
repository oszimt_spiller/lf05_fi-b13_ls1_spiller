﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static void warte(int millisekunde) {
		// Ausgelagerte warte Funktion mit Wartezeit als Argument
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

	public static double fahrkartenbestellungErfassen() {
		// Erfasst die Eingabe bzgl. Anzahl der Tickets und dem Preis und gibt den
		// zuzahlenden Betrag zurueck

		double ticketpreis = 0;
		short anzahlTickets;
        //Array für den Fahrkartenpreis
		double[] preis = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.0, 9.60, 23.50, 24.30, 24.90

		};
        //Array für die Fahrkartenbezeichnung
		String[] name = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
				"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC"

		};

		Scanner tastatur = new Scanner(System.in);

		// Konsolenstyle
		System.out.println("Fahrkartenbestellvorgang: ");
		for (int i = 0; i < 25; i++) {
			System.out.print("=");
			// Wartemethode wird mit 250ms aufgerufen
			warte(150);
		}
		// Erstellung des Menüs und Aufforderung zur Eingabe der Auswahl
		System.out.println("\n");
		System.out.println(
				"Wählen Sie ihre Wunschfahrkarte für Berlin AB aus, dazu geben Sie bitte die Auswahlnummer ein.\n");

		// Kopfzeile
		System.out.printf("%-13s", "Auswahlnummer");
		System.out.print("|");
		System.out.printf("%-35s", "Bezeichnung");
		System.out.print("|");
		System.out.printf("%10s%n", "Preis in Euro");
		System.out.printf("%.63s%n",
				"--------------------------------------------------------------------------------------------------------");
		// Erzeugung der eigentlichen Einträge, passt sich bei Änderung der Arrays an
		for (int i = 0; i < preis.length; i++) {
			System.out.printf("%-13d", i + 1);
			System.out.print("|");
			System.out.printf("%-35s", name[i]);
			System.out.print("|");
			System.out.printf("%10.2f%n", preis[i]);
		}

		// Auswahlnummer in Arrayindex umwandeln und üngültige Auswahl abfangen
		boolean gueltigeauswahl = false;
		while (gueltigeauswahl == false) {
			int auswahl = tastatur.nextInt();
			if (auswahl <= preis.length && auswahl >= 1) {
				ticketpreis = preis[auswahl - 1];
				gueltigeauswahl = true;
			} else {
				System.out.println("Ihre Auswahlnummer existiert nicht, bitte versuchen Sie es erneut!");

			}
		}

		// Anzahl der Tickets
		System.out.print("Bitte geben Sie die Anzahl der Tickets ein! ");
		anzahlTickets = tastatur.nextShort();

		double zuZahlen = anzahlTickets * ticketpreis;
		return zuZahlen;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		// Methode mit dem zuzahlenden Gesamtbetrag als Argument, die den
		// Rueckgabebetrag returned
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;

		while (eingezahlterGesamtbetrag < zuZahlen) {

			double restbetrag = zuZahlen - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f Euro%n", restbetrag);

			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		// Rueckgabebetrag wird ausgerechnet
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
		return rueckgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		// Hier habe ich einfach die schon vorhandene for-Schleife aus dem Frueherem
		// Arbeitsauftrag in eine Methode gesteckt
		// Ein Rückgabewert bzw. Argumente sind nicht nötig, es ist ja nur eine
		// Konsolenspielerei ^.^

		System.out.println("\nFahrscheine werden ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			// Wartemethode wird mit 250ms aufgerufen
			warte(250);
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {
		// Methode die den rueckgabebetrag als Argument bekommt und die Münzen ausgibt,
		// die zurueckgegeben werden sollen
		if (rueckgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO%n", rueckgabebetrag);

			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rueckgabebetrag -= 2.0;
			}

			while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rueckgabebetrag -= 1.0;

			}

			while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabebetrag -= 0.5;
			}

			while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabebetrag -= 0.2;
			}

			while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabebetrag -= 0.05;
			}
			while (rueckgabebetrag >= 0.03)// 3 CENT-Münzen
			{
				muenzeAusgeben(3, "CENT");
				rueckgabebetrag -= 0.03;
			}
			while (rueckgabebetrag >= 0.02)// 2 CENT-Münzen
			{
				muenzeAusgeben(2, "CENT");
				rueckgabebetrag -= 0.02;
			}
			while (rueckgabebetrag >= 0.01)// 1 CENT-Münzen
			{
				muenzeAusgeben(1, "CENT");
				rueckgabebetrag -= 0.01;
			}
		}

	}

	public static void main(String[] args) {
		// initialisiere weitereKaeufe auf true, damit der Benutzer später entscheiden
		// kann, ob er weitere Karten kaufen will
		boolean weitereKaeufe = true;
		while (weitereKaeufe) {
			Scanner tastatur = new Scanner(System.in);
			// Eingabe bzw. Erfassung des zuzahlendem Betrags durch die Funktion
			double zuZahlenderBetrag = fahrkartenbestellungErfassen();

			// Geldeinwurf und Rueckgeldberechnung
			// Die Funktion bekommt den zuzahlenden Betrag und returned den Rueckgabebetrag
			double rueckgabe = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrscheinausgabe
			// -----------------
			fahrkartenAusgeben();

			// Fix für den Rundungsfehler bei double Berechnung, da sonst die Muenzaufgabe
			// am
			// Ende falsch ist!
			rueckgabe += 0.00001;

			// Funktion für die Rückgabe der geldmuenzen mit dem Rueckgabebetrag als
			// Argument
			rueckgeldAusgeben(rueckgabe);
			// Frage den Benutzer ob er noch mehr Karten kaufen möchte, falls nicht wird
			// weitereKaeufe false und die Whileschleife beendet
			System.out.println(
					"Falls sie noch weitere Fahrkarten kaufen möchten, geben sie bitte eine 1 ein oder eine 2 falls nicht.");
			int entscheidung = tastatur.nextInt();
			if (entscheidung == 2) {
				weitereKaeufe = false;
			}
		}
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
}

/*
 * 1. Deutlich weniger Schreibaufwand. Bessere Skalierbarkeit wenn man neue
 * Karten hinzufügen möchte, also im Endeffekt weniger Wartungsarbeiten während
 * des Betriebs. Ein Array hat von sich aus schon ein Index, wodurch die Abfrage
 * nach der Auswahl einfacher ist. 
 * 3. Das mit dem hinzufügen neuer Einträg im
 * Array habe ich schon bei meiner ersten Implementierung in Aufgabe 1 beachtet,
 * deshalb habe ich auch die for-Schleife mit der Arraylänge zur Erstellung der
 * Menüeinträge. Dadurch hat man verglichen mit der alten implementierung einen
 * übersichtlicheren Programmiercode und eine angenehmere Skalierbarkeit
 * 
 * 
 */