import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie bitte einen begrenzenden Wert ein!");
		int begrenzer = tastatur.nextInt();
		// a 1 + 2 + 3 + 4 +...+ n
		int a_ergebnis = 0;
		int b_ergebnis = 0;
		// c startet bei 1 weil erster step 0*2 +1 ist
		int c_ergebnis = 1;
		for (int i = begrenzer; i > 0; i--) {
			a_ergebnis = a_ergebnis + i;
		}
		System.out.println("Die Summe f�r A betr�gt: " + a_ergebnis);

		// bb 2 + 4 + 6 +...+ 2n
		for (int i = begrenzer; i > 0; i--) {
			b_ergebnis = b_ergebnis + i * 2;
		}
		System.out.println("Die Summe f�r B betr�gt: " + b_ergebnis);
		// c 1 + 3 + 5 +...+ (2n+1)
		for (int i = begrenzer; i > 0; i--) {
			c_ergebnis = c_ergebnis + i * 2 + 1;
		}
		System.out.print("Die Summe f�r C betr�gt: " + c_ergebnis);
	}

}
