import java.util.Scanner;

public class Aufgabe1 {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte geben an, bis zu welcher nat�rlichen Zahl gez�hlt werden soll.");
		int eingabezahl = tastatur.nextInt();
		System.out.print(
				"Falls von 1 angefangen werden soll zu z�hlen, geben sie ein ja ein. Sonst geben sie bitte ein nein ein und es wird von der eingegeben Zahl aus nach unten gez�hlt");
		String entscheidung = tastatur.next();
		
		if (entscheidung.equals("ja")) {
			for (int i = 0; i < eingabezahl; i++) {
				System.out.println(i + 1);
			}
		} else if (entscheidung.equals("nein") ) {

			for (int i = eingabezahl; i > 0; i--) {
				System.out.println(i);
			}
		}
	}
}
