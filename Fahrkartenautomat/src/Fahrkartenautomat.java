﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double ticketpreis;
       //hinzugefügte Variable
       short anzahlTickets;
       
       //Ticketpreis
       System.out.print("Ticketpreis (EURO): ");
       ticketpreis = tastatur.nextDouble();
       
       //Anzahl der Tickets
       System.out.print("Anzahl der Tickets: ");
       anzahlTickets = tastatur.nextShort();
       
       
       //der neue zuzahlende Betrag ergibt sich nun aus der Anzahl der Tickets und dem Ticketpreis
       zuZahlenderBetrag = anzahlTickets * ticketpreis;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   //Erster Formatierungsfehler behoben durch %.2f (zwei Nachkommastellen)
    	   double restbetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
    	   System.out.printf("Noch zu zahlen: %.2f Euro%n", restbetrag);
    	   
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrscheine werden ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       //Aufgrund eines Rundungsfehler bei der Rechnung mit zwei Doubles, wurden die falschen Münzen zurückgegeben.
       //Hier habe ich den Rundungsfehler manuell behoben, durch das Addieren eines sehr kleinen Wertes.
       rückgabebetrag += 0.00001;
       
       if(rückgabebetrag > 0.0)
       {
    	   //zweiter Formatierungsfehler, wieder auf zwei Nachkommastellen %.2f
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO%n", rückgabebetrag);
    	   
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
            
           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
          
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
	          
           }
           
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
          
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
          
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}

// zu 1.
//Eigentlich nur Doubles. Ich habe noch ein short hinzugefügt für die Anzahl der Tickets

//zu 2.
// -= Das Abziehen des Wertes nach dem = , Ich habe noch ein += hinzugefügt um den Fehler bei der Rückgabe der Münzen zu beheben
//-, +, <, >, <=, >=,* sind selbsterklärend. ++ erhöht um eins

//zu 5.
//Für die neue Variable anzahlTickets habe ich mich für short entschieden, da eventuell jemand mehr als 127 Tickets kaufen könnte 
//und ein byte nicht ausreichen könnte.

//zu 6.
// Bei der Berechnung anzahlTickets*Ticketpreis findet eine Multiplikation zwischen dem short anzahlTickets und dem double Ticketpreis statt
// Das Ergebnis der Multiplikation wird hier dann als double zuZahlenderBetrag abgespeichert.
