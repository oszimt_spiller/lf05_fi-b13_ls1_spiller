import java.util.ArrayList;
import java.util.Arrays;

public class Raumschiff {
	// Attribute
	private String schiffname;
	private double energieversorgung;
	private double schutzschild;
	private double lebenserhaltungssysteme;
	private double huelle;
	private int repAndroide;
	private int photonentorpedo;
	private Ladung[] ladungsverzeichnis;

	// Konstruktoren
	/**(Effekt.:) Konstruktor ohne Parameter */
	public Raumschiff() {

	}
	/**(Effekt.:) Konstruktor mit Parameter
	 * @param schiffname   der Name des Schiffs
	 * @param energieversorgung die Energiemenge des Schiffs
	 * @param schutzschild Schutzschildzustand in
	 * @param lebenserhaltungssysteme Zustand in % der Lebenserhaltungssysteme
	 * @param huelle Zustand der Huelle in %
	 * @param repAndroide Anzahl der Reperaturandroide
	 * @param photonentorpedo Anzahl der Photonentorpedos
	 * @param ladungsverzeichnis das Ladungsverzeichnis */
	public Raumschiff(String schiffname, double energieversorgung, double schutzschild, double lebenserhaltungssysteme,
			double huelle, int repAndroide, int photonentorpedo, Ladung[] ladungsverzeichnis) {

		this.schiffname = schiffname;
		this.energieversorgung = energieversorgung;
		this.schutzschild = schutzschild;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.huelle = huelle;
		this.repAndroide = repAndroide;
		this.photonentorpedo = photonentorpedo;
		this.ladungsverzeichnis = ladungsverzeichnis;

	}

	// Getter und Setter
	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}

	public String getSchiffname() {
		return this.schiffname;
	}

	public void setEnergieversorgung(double energieversorgung) {
		this.energieversorgung = energieversorgung;
	}

	public double getEnergieversorgung() {
		return this.energieversorgung;
	}

	public void setSchutzschild(double schutzschild) {
		this.schutzschild = schutzschild;
	}

	public double getSchutzschild() {
		return this.schutzschild;
	}

	public void setLebenserhaltungssysteme(double lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}

	public double getLebenserhaltungssysteme() {
		return this.lebenserhaltungssysteme;
	}

	public void setHuelle(double huelle) {
		this.huelle = huelle;
	}

	public double getHuelle() {
		return this.huelle;
	}

	public void setRepAndroide(int repAndroide) {
		this.repAndroide = repAndroide;
	}

	public int getRepAndroide() {
		return this.repAndroide;
	}

	public void setPhotonentorpedo(int photonentorpedo) {
		this.photonentorpedo = photonentorpedo;
	}

	public int getPhotonentorpedo() {
		return this.photonentorpedo;
	}

	public void setLadungsverzeichnis(Ladung[] ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public Ladung[] getLadungsverzeichnis() {
		return this.ladungsverzeichnis;
	}

	// Andere Methoden
	/**(Effekt.:) Ladung wird dem Ladungsverzeichnis hinzugefuegt, dafuer wird
	 * der Array in eine Arraylist umgewandelt, die Ladung hinzugef�gt und wieder
	 * zum Array konvertiert.
	 * @param lad die Ladung die zum Ladungsverzeichnis hinzugef�gt werden soll*/
	public void addLadung(Ladung lad) {
		// Zuerst den Array in ein Listenformat bringen, danach das Objekt lad
		// hinzuf�gen
		// und wieder zum Array konvertieren
		ArrayList<Ladung> arrayList = new ArrayList<Ladung>(Arrays.asList(ladungsverzeichnis));
		arrayList.add(lad);
		ladungsverzeichnis = arrayList.toArray(ladungsverzeichnis);
	}

	// Zustand des Schiffes ausgeben.
	/**(Effekt.:) Es werden alle Attribute und ihre Werte des Raumschiffs ausgegeben.*/
	public void showStatus() {
		System.out.print("Statusbericht \n" + "Name: " + schiffname + "\nEnergieversorgung: " + energieversorgung + "%"
				+ "\nSchutzschild: " + schutzschild + "%" + "\nLebenserhaltungssysteme: " + lebenserhaltungssysteme
				+ "%" + "\nHuelle: " + huelle + "%" + "\nRepandroide: " + repAndroide + "\nPhotonentorpedo: "
				+ photonentorpedo + "\n\n");
	}

	// Ladung ausgeben durch toString auf alle Elemente des Ladungsarrays
	/**(Effekt.:) Das Ladungsverzeichnis wird durchiteriert und alle Ladungen werden ausgegeben*/
	public void showLadungsverzeichnis() {
		int i = 0;
		while (i < ladungsverzeichnis.length) {
			System.out.print(ladungsverzeichnis[i].toString() + "\n");
			i++;
		}
	}

	// Treffer Methode
	/**(Effekt.:) Notiert einen Treffer und sendet das als Broadcast. Au�erdem
	 * wird das Schutzschild reduziert und bei einer Schutzschildzerstoerung
	 * kommt ein weiterer Broadcast.
	 * @param rx das getroffene Raumschiff */
	private void treffer(Raumschiff rx) {
		sendeBroadcast(rx.getSchiffname() + " wurde getroffen!\n");
		rx.setSchutzschild(rx.getSchutzschild() - 50);
		if (rx.getSchutzschild() <= 0) {
			rx.setHuelle(rx.getHuelle() - 50);
			rx.setEnergieversorgung(rx.getEnergieversorgung() - 50);
		}
		if (rx.getHuelle() <= 0) {
			rx.sendeBroadcast("Meine Lebenserhaltungssysteme wurden zerstoert!");
		}
	}

	// Torpedos abfeuern
	/**(Effekt.:) Schie�t ein Torpedo auf das Raumschiff oder sendet einen Broadcast,
	 * falls keine Torpedos vorhanden sind. Bei einem Schuss wird treffer() aufgerufen.
	 * @param rx Das Ziel des Torpedos (Raumschiff) */
	public void schiesseTorpedos(Raumschiff rx) {
		if (this.photonentorpedo <= 0) {
			sendeBroadcast("-=*Click*=-");
		} else {
			photonentorpedo--;
			sendeBroadcast("Photonentorpedo abgeschossen");
			treffer(rx);
		}
	}

	// Nachricht an alle senden
	/**(Effekt.:) Sendet den String als Broadcast und f�gt diesem dem Broadcastkommunikator
	 * hinzu.
	 * @param broad Der Inhalt des Broadcasts */
	public void sendeBroadcast(String broad) {
		System.out.print(this.schiffname + ": " + broad + "\n");
		Test.broadcastKommunikator.add("| " + this.schiffname + ": " + broad + " |");
	}

	// Phaserkanone abfeuern
	/**(Effekt.:) Schie�t eine Phaserkanone auf das Raumschiff oder sendet einen Broadcast,
	 * falls keine Energie daf�r vorhanden ist. Bei einem Schuss wird treffer() aufgerufen.
	 * @param rx Das Ziel des Schusses (Raumschiff) */
	public void schiessePhaserkanone(Raumschiff rx) {
		if (this.energieversorgung < 50) {
			sendeBroadcast("-=*Click*=-");
		} else {
			energieversorgung -= 50;
			sendeBroadcast("Phaserkanone abgeschossen");
			treffer(rx);
		}
	}
	/**(Effekt.:) Gibt den Inhalt des Broadcastkommunikators als String aus*/
	public void broadKommAusgeben() {
		System.out.print(Test.broadcastKommunikator.toString());
	}

}
