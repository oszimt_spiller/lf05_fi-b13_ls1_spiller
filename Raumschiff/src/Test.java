import java.util.ArrayList;

public class Test {
	// Ich habe die Arraylist hier abgestellt, sp�ter m�sste man noch den Ort und
	// die Klasse Raumschiff anpassen, wenn man die eigentliche Mainklasse hat
	public static ArrayList<String> broadcastKommunikator = new ArrayList<String>();

	public static void main(String[] args) {
		
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung l3 = new Ladung("Borg Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasmawaffe", 50);
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);

		// Leeren Ladungsarray erstellen, da mein Konstruktor solch einen braucht.
		Ladung[] ladungRohling = new Ladung[0];

		Raumschiff r1 = new Raumschiff("IKS Heigh'ta", 100, 50, 100, 100, 2, 0, ladungRohling);
		Raumschiff r2 = new Raumschiff("IRW Khazara", 100, 50, 50, 100, 2, 2, ladungRohling);
		Raumschiff r3 = new Raumschiff("Ni'Var", 80, 80, 100, 50, 5, 0, ladungRohling);

		r1.schiesseTorpedos(r2);
		r2.schiessePhaserkanone(r1);
		r3.sendeBroadcast("Gewalt ist nicht logisch");
		r1.showStatus();
		r1.showLadungsverzeichnis();
		r1.schiesseTorpedos(r2);
		r1.schiesseTorpedos(r2);
		r1.showStatus();
		r1.showLadungsverzeichnis();
		r2.showStatus();
		r2.showLadungsverzeichnis();
		r3.showStatus();
		r3.showLadungsverzeichnis();
		r1.broadKommAusgeben();

	}

}
