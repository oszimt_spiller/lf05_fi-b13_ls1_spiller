
public class Ladung {
	private String name;
	private int anzahl;

	/**(Effekt.:) Konstruktor ohne Parameter */
	public Ladung() {

	}

	/**(Effekt.:) Konstruktor mit Parameter
	 * @param name   der Name der Ladung
	 * @param anzahl die Anzahl der Ladung */
	public Ladung(String name, int anzahl) {
		this.name = name;
		this.anzahl = anzahl;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public String getName() {
		return this.name;
	}

	public int getAnzahl() {
		return this.anzahl;
	}
	
	/**(Effekt.:) gibt den Inhalt des Ladungsarray als String aus,
	 *  inklusive der anzahl */
	public String toString() {
		return "Ladung" + "[Name: " + name + ", Anzahl: " + anzahl + "]";
	}

}
